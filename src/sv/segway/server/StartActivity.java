package sv.segway.server;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class StartActivity extends Activity implements View.OnClickListener {

    private Button send;
    private EditText request;
    static final String ATTRIBUTE_NAME_REQUEST = "request";
    static final String ATTRIBUTE_NAME_ANSWER = "answer";
    SimpleAdapter sAdapter;
    ArrayList<Map<String, Object>> data;
    Map<String, Object> m;
    private ListView lvSimple;
    BluetoothAdapter mBluetoothAdapter;
    Handler mHandler;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String MYNAME ="SvSegway";
    private static final int MESSAGE_READ =123;
    private static final int SYSTEM_READ = 124;
    ConnectedThread manageConnectedSocket;
    private String lastCommand;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        lvSimple = (ListView) findViewById(R.id.listViewCommand);
        send = (Button) findViewById(R.id.sendButton);
        send.setOnClickListener(this);
        request = (EditText) findViewById(R.id.requestText);
        data = new ArrayList<Map<String, Object>>();

        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                switch (msg.what) {
                    case MESSAGE_READ:
                        String mAnswer = msg.obj.toString();
                        ShowAnswer(lastCommand,mAnswer);
                        break;
                    case SYSTEM_READ:
                        ShowAnswer("-- System --", msg.obj.toString());
                    default:
                }


            }
        };

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            m = new HashMap<String, Object>();
            m.put(ATTRIBUTE_NAME_REQUEST, "-- System --");
            m.put(ATTRIBUTE_NAME_ANSWER, "Device does not support Bluetooth");
            data.add(m);
        }
        if (!mBluetoothAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
        }
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
        m = new HashMap<String, Object>();
        m.put(ATTRIBUTE_NAME_REQUEST, "-- System --");
        m.put(ATTRIBUTE_NAME_ANSWER, "Discoverable");
        data.add(m);
        m = new HashMap<String, Object>();
        m.put(ATTRIBUTE_NAME_REQUEST, "-- System --");
        m.put(ATTRIBUTE_NAME_ANSWER, "Server running");
        data.add(m);
        AcceptThread ac = new AcceptThread();
        Collections.reverse(data);
        String[] from = { ATTRIBUTE_NAME_REQUEST, ATTRIBUTE_NAME_ANSWER };
        int[] to = { R.id.requestItem, R.id.answerItem };
        sAdapter = new SimpleAdapter(this, data, R.layout.item, from, to);
        lvSimple.setAdapter(sAdapter);
        registerForContextMenu(lvSimple);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sendButton:
                if(request.length()>0){
                    command(request.getText().toString());
                    request.setText("");
                }
                break;
            default:
        }
    }

    private void command(String comm) {
        lastCommand = comm;
        if(manageConnectedSocket != null){
            manageConnectedSocket.write(comm.getBytes());
        }
        else{
            ShowAnswer("-- System --", "Client not found");
        }

        /*
        Thread t = new Thread(new Runnable() {
            Message msg;
            byte[] file;
            Random rand = new Random();

            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    mHandler.obtainMessage(MESSAGE_READ, 5, -1, "Answer: " + lastCommand + "\n " + rand.nextInt(100000)).sendToTarget();
                } catch (InterruptedException e) {
                e.printStackTrace();
                }
            }
        });
        t.start();
        */


    }

    private void ShowAnswer(String showRequest, String showAnswer) {
        Collections.reverse(data);
        m = new HashMap<String, Object>();
        m.put(ATTRIBUTE_NAME_REQUEST, showRequest);
        m.put(ATTRIBUTE_NAME_ANSWER, "answer: "+showAnswer);
        data.add(m);
        Collections.reverse(data);
        sAdapter.notifyDataSetChanged();
    }


    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket,
            // because mmServerSocket is final
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(MYNAME, MY_UUID_SECURE);
            } catch (IOException e) { }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    manageConnectedSocket = new ConnectedThread(socket);
                    mHandler.obtainMessage(SYSTEM_READ, -1, -1, "Client connected").sendToTarget();
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        /** Will cancel the listening socket, and cause the thread to finish */
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) { }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
}
